FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive \
    PATH="$PATH:/root/.local/bin" \
    POETRY_VERSION="1.7.1" \
    POETRY_CACHE_DIR="/var/cache/pypoetry"

RUN apt-get update \
    && apt-get install -y software-properties-common \
    && add-apt-repository "ppa:deadsnakes/ppa" \
    && apt-get purge -y software-properties-common \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /file_shara
COPY ./poetry.lock ./pyproject.toml ./

RUN apt-get update \
    && apt-get install --no-install-recommends -y postgresql postgresql-client python3.11 curl python3.11-dev \
    && curl -sSL https://install.python-poetry.org | POETRY_VERSION=${POETRY_VERSION} python3.11 - \
    && poetry config virtualenvs.create false \
    && poetry install --no-interaction --no-ansi --no-root --no-dev \
    && apt-get purge -y curl python3.11-dev \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./app ./app

EXPOSE 8000
ENV PYTHONPATH "$PWD:$PYTHONPATH"

ENTRYPOINT ["hypercorn", "--reload", "--bind", "0.0.0.0:8000", "--workers", "1", "app/main:app"]
from passlib.context import CryptContext

passwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password, hashed_password):
    return passwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return passwd_context.hash(password)

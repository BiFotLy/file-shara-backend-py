from fastapi import APIRouter, Body, Depends, HTTPException, status
from sqlalchemy.orm import Session

from app.db.db_session import get_ses
from app.db.models import User as pg_User

from ..login.schemas import User
from ..utils import get_password_hash
from .dependencies import get_current_admin
from .schemas import UserPasswordPayload

router = APIRouter()

# TODO: add path parameter for 'username' with proper validation


@router.post(
    "/users",
    response_model=User,
    dependencies=[Depends(get_current_admin)],
)
def add_new_user(
    payload: UserPasswordPayload = Body(),
    ses: Session = Depends(get_ses),
):
    user = ses.get(pg_User, payload.username)
    if user:
        raise HTTPException(
            status.HTTP_409_CONFLICT,
            detail="User already exists",
        )

    password_hash = get_password_hash(payload.password)
    ses.add(
        pg_User(
            username=payload.username,
            password_hash=password_hash,
        )
    )
    ses.commit()

    return {"username": payload.username}


@router.put(
    "/users/password",
    response_model=User,
    dependencies=[Depends(get_current_admin)],
)
async def update_user_password(
    payload: UserPasswordPayload = Body(),
    ses: Session = Depends(get_ses),
):
    user = ses.get(pg_User, payload.username)
    if not user:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            detail="User not found",
        )

    user.password_hash = get_password_hash(payload.password)
    ses.commit()

    return {"username": payload.username}

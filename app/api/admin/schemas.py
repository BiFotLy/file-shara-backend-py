from pydantic import Field

from ..login.schemas import User


class UserPasswordPayload(User):
    password: str = Field(pattern=r"^[a-zA-Z0-9_-]{1,50}$")

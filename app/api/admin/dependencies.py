from typing import Annotated

from fastapi import Depends, HTTPException, status

from ..login.dependencies import get_current_user
from ..login.schemas import User


def get_current_admin(
    user: Annotated[User, Depends(get_current_user)],
):
    if not user.username == "admin":
        raise HTTPException(
            status.HTTP_403_FORBIDDEN,
            detail="User has insufficient permissions",
        )
    return user

from pydantic import UUID4, BaseModel, HttpUrl


class ShareLinkResponse(BaseModel):
    file_uuid: UUID4
    url: HttpUrl | None

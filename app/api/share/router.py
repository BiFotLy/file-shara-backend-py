from typing import Annotated, List
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, Request, status
from sqlalchemy import exists, select
from sqlalchemy.orm import Session

from app.api.login.dependencies import get_current_user
from app.api.login.schemas import User
from app.db.db_session import get_ses
from app.db.models import File as pg_File
from app.db.models import User as pg_User
from app.utils import gen_rand_chars

from .schemas import ShareLinkResponse

router = APIRouter()


@router.post(
    "/files/{file_uuid}/share",
    response_model=ShareLinkResponse,
    dependencies=[Depends(get_current_user)],
)
async def create_share_link(
    file_uuid: UUID,
    ses: Annotated[Session, Depends(get_ses)],
    request: Request,
):
    pg_file = ses.scalar(select(pg_File).where(pg_File.uuid == file_uuid))
    if not pg_file:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            detail="File not found",
        )

    if pg_file.share_link_path:
        raise HTTPException(
            status.HTTP_409_CONFLICT,
            detail="Link already exists",
        )

    while True:
        link_path = gen_rand_chars(30)
        link_exists = ses.scalar(
            select(exists().where(pg_File.share_link_path == link_path))
        )
        if not link_exists:
            pg_file.share_link_path = link_path
            ses.commit()
            break

    return {
        "file_uuid": file_uuid,
        "url": str(
            request.url_for("download_from_share_link", path=pg_file.share_link_path)
        ),
    }


@router.get(
    "/share_links",
    response_model=List[ShareLinkResponse],
)
async def get_all_share_links(
    user: Annotated[User, Depends(get_current_user)],
    ses: Annotated[Session, Depends(get_ses)],
    request: Request,
):
    pg_files = ses.get_one(pg_User, user.username).files

    # Dirty hack
    download_path = str(request.url_for("download_from_share_link", path="a"))[:-1]
    return [
        {"file_uuid": file.uuid, "url": download_path + file.share_link_path}
        for file in pg_files
        if file.share_link_path
    ]


@router.delete(
    "/files/{file_uuid}/share",
    response_model=ShareLinkResponse,
    dependencies=[Depends(get_current_user)],
)
async def delete_share_link(
    file_uuid: UUID,
    ses: Annotated[Session, Depends(get_ses)],
):
    pg_file = ses.scalar(select(pg_File).where(pg_File.uuid == file_uuid))
    if not pg_file:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            detail="File not found",
        )

    pg_file.share_link_path = None
    ses.commit()

    return {"file_uuid": pg_file.uuid, "url": None}

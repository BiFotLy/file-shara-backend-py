from datetime import datetime, timedelta
from typing import Annotated

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from sqlalchemy.orm import Session

from app.api.login.schemas import TokenData, User
from app.api.utils import verify_password
from app.db.db_session import get_ses
from app.db.models import User as pg_User
from app.utils import get_env

SECRET_KEY = get_env("SECRET_KEY")
CRYPT_ALGO = "HS256"
ACCESS_TOKEN_EXPIRE_HOURS = 24

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/login/token")


def get_user(ses: Session, username: str) -> pg_User | None:
    return ses.get(pg_User, username)


def authenticate_user(ses: Session, username: str, password: str):
    user = get_user(ses, username)
    if not user:
        return False
    if not verify_password(password, user.password_hash):
        return False
    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=CRYPT_ALGO)
    return encoded_jwt


credentials_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"},
)


def get_auth_user(
    ses: Annotated[Session, Depends(get_ses)],
    token: Annotated[str, Depends(oauth2_scheme)],
):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[CRYPT_ALGO])
        username = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
        if token_data.username is None:
            raise credentials_exception
    except JWTError as e:
        raise credentials_exception from e
    user = get_user(ses, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


def get_current_user(user: Annotated[User, Depends(get_auth_user)]):
    return user

from pydantic import UUID4, BaseModel, HttpUrl


class FileInfoResponse(BaseModel):
    filename: str
    uuid: UUID4
    share_url: HttpUrl | None = None

import os
import shutil
from typing import Annotated, List
from uuid import UUID

from fastapi import APIRouter, Depends, File, HTTPException, Request, UploadFile, status
from sqlalchemy import select
from sqlalchemy.orm import Session
from starlette.responses import FileResponse

from app.db.db_session import get_ses
from app.db.models import File as pg_File
from app.db.models import User as pg_User

from ..login.dependencies import get_current_user
from ..login.schemas import User
from .schemas import FileInfoResponse

router = APIRouter()


# TODO: add digest checking


@router.post("", response_model=FileInfoResponse)
async def upload_file(
    file: Annotated[UploadFile, File()],
    user: Annotated[User, Depends(get_current_user)],
    ses: Annotated[Session, Depends(get_ses)],
):
    pg_file = ses.get(pg_File, (file.filename, user.username))
    if pg_file:
        raise HTTPException(
            status.HTTP_409_CONFLICT,
            detail="File already exists",
        )

    pg_file = pg_File(filename=file.filename, username=user.username)
    ses.add(pg_file)
    ses.commit()

    dst = f"uploaded_files/{pg_file.uuid}"
    try:
        with open(dst, "wb") as buffer:
            shutil.copyfileobj(file.file, buffer)
    finally:
        file.file.close()

    return {
        "filename": file.filename,
        "uuid": pg_file.uuid,
    }


@router.get("", response_model=List[FileInfoResponse])
async def get_all_files(
    user: Annotated[User, Depends(get_current_user)],
    ses: Annotated[Session, Depends(get_ses)],
    request: Request,
):
    pg_files = ses.get_one(pg_User, user.username).files

    # Dirty hack
    download_path = str(request.url_for("download_from_share_link", path="a"))[:-1]
    response = [
        {
            **file.__dict__,
            **{
                "share_url": download_path + file.share_link_path
                if file.share_link_path
                else None
            },
        }
        for file in pg_files
    ]
    response.sort(key=lambda d: d["filename"])
    return response


@router.delete(
    "/{file_uuid}",
    dependencies=[Depends(get_current_user)],
)
async def delete_file(
    file_uuid: UUID,
    ses: Annotated[Session, Depends(get_ses)],
):
    pg_file = ses.scalar(select(pg_File).where(pg_File.uuid == file_uuid))
    if not pg_file:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            detail="File not found",
        )

    filepath = f"uploaded_files/{pg_file.uuid}"
    os.remove(filepath)

    ses.delete(pg_file)
    ses.commit()

    return {"detail": "File deletion success"}


@router.get(
    "/{file_uuid}/download",
    response_class=FileResponse,
)
async def download_file(
    file_uuid: UUID,
    user: Annotated[User, Depends(get_current_user)],
    ses: Annotated[Session, Depends(get_ses)],
):
    pg_file = ses.scalar(
        select(pg_File).where(
            pg_File.username == user.username, pg_File.uuid == file_uuid
        )
    )
    if not pg_file:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            detail="File not found",
        )

    return FileResponse(
        f"uploaded_files/{pg_file.uuid}",
        media_type="application/octet-stream",
        filename=pg_file.filename,
    )

from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy import select
from sqlalchemy.orm import Session
from starlette.responses import FileResponse

from app.db.db_session import get_ses
from app.db.models import File as pg_File

router = APIRouter()


@router.get("/{path}", response_class=FileResponse)
async def download_from_share_link(
    path: str, ses: Annotated[Session, Depends(get_ses)]
):
    pg_file = ses.scalar(select(pg_File).where(pg_File.share_link_path == path))
    if not pg_file:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            detail="File not found",
        )

    return FileResponse(
        f"uploaded_files/{pg_file.uuid}",
        media_type="application/octet-stream",
        filename=pg_file.filename,
    )

from fastapi import APIRouter

from .admin.router import router as admin_router
from .files.router import router as files_router
from .login.router import router as login_router
from .public_download.router import router as download_router
from .share.router import router as share_router

api_router = APIRouter()

api_router.include_router(login_router, prefix="/login", tags=["login"])
api_router.include_router(files_router, prefix="/files", tags=["files"])
api_router.include_router(admin_router, prefix="/admin", tags=["admin"])
api_router.include_router(share_router, tags=["share"])

root_router = APIRouter()
root_router.include_router(
    download_router, prefix="/download", tags=["public_download"]
)

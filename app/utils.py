import os
import pathlib
import random
import string

BASE_DIR = pathlib.Path(__file__).parent.absolute()


def gen_rand_chars(length: int) -> str:
    rand_chars = string.ascii_letters + "_+-"
    return "".join(random.choice(rand_chars) for _ in range(length))


def get_env(name: str) -> str:
    env_var = os.getenv(name)
    if env_var is None:
        raise EnvironmentError(f"Env {name} can't be None")
    return env_var

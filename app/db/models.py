import uuid
from typing import Annotated, Any, List, Optional

from sqlalchemy import BigInteger, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship
from sqlalchemy.orm.exc import DetachedInstanceError

BigInt = Annotated[int, "BigInt"]


class Base(DeclarativeBase):
    type_annotation_map = {BigInt: BigInteger()}

    def _repr(self, **fields: Any) -> str:
        field_strings = []
        at_least_one_attached_attribute = False
        for key, field in fields.items():
            try:
                field_strings.append(f"{key}={field!r}")
            except DetachedInstanceError:
                field_strings.append(f"{key}=DetachedInstanceError")
            else:
                at_least_one_attached_attribute = True
        if at_least_one_attached_attribute:
            return f"{self.__class__.__name__}({', '.join(field_strings)})"
        return f"{self.__class__.__name__}"


class User(Base):
    __tablename__ = "users"

    username: Mapped[str] = mapped_column(primary_key=True)
    password_hash: Mapped[str]

    files: Mapped[List["File"]] = relationship(back_populates="user")

    def __repr__(self) -> str:
        return self._repr(
            username=self.username,
            password_hash=self.password_hash,
        )


class File(Base):
    __tablename__ = "files"

    filename: Mapped[str] = mapped_column(primary_key=True)
    username: Mapped[str] = mapped_column(
        ForeignKey("users.username"), primary_key=True
    )
    uuid: Mapped[UUID] = mapped_column(
        UUID(as_uuid=True), default=uuid.uuid4, unique=True, index=True
    )
    share_link_path: Mapped[Optional[str]]

    user: Mapped["User"] = relationship(back_populates="files")

    def __repr__(self) -> str:
        return self._repr(
            filename=self.filename,
            username=self.username,
            uuid=self.uuid,
            share_link_path=self.share_link_path,
        )

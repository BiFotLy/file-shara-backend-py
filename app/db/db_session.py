from functools import cache

from sqlalchemy import URL, create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker

from app.utils import get_env


@cache
def get_pgsql_engine() -> Engine:
    pg_user = get_env("PG_USER")
    pg_passwd = get_env("PG_PASSWD")
    pg_db = get_env("PG_DB")
    pg_container_name = get_env("PG_CONTAINER_NAME")

    return create_engine(
        URL.create(
            "postgresql+psycopg",
            username=pg_user,
            password=pg_passwd,
            host=pg_container_name,
            port=5432,
            database=pg_db,
        ),
        echo=False,
    )


SessionLocal = sessionmaker(bind=get_pgsql_engine(), autocommit=False, autoflush=False)


def get_ses():
    ses = SessionLocal()
    try:
        yield ses
    finally:
        ses.close()

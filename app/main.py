import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session

from app.api.router import api_router, root_router
from app.api.utils import get_password_hash
from app.db.db_session import get_pgsql_engine
from app.db.models import Base, User


def add_admin(
    password: str,
):
    with Session(get_pgsql_engine()) as ses:
        admin = ses.get(User, "admin")
        if not admin:
            ses.add(
                User(
                    username="admin",
                    password_hash=get_password_hash(password),
                )
            )
            ses.commit()


def get_app():
    fastapi = FastAPI()

    fastapi.include_router(root_router)
    fastapi.include_router(api_router, prefix="/api")

    origins = ["http://localhost:5173"]  # Default port for Vite
    fastapi.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    Base.metadata.create_all(get_pgsql_engine())
    add_admin("password")

    # TODO: move hardcoded dirname to .env
    os.makedirs("uploaded_files", mode=0o644, exist_ok=True)

    return fastapi


app = get_app()
